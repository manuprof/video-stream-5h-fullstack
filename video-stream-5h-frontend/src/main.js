import { createApp } from 'vue'
import App from './App.vue'
import Home from './Home.vue'
import About from './About.vue'
import WatchVideo from './WatchVideo.vue'
import { createRouter, createWebHistory } from 'vue-router';

const routes = [
    { path: '/', component: Home },
    { path: '/about', component: About },
    { path: '/watch?id=:id', name: 'watch', component: WatchVideo },
    //{ path: '/video-list', component: null },
]

// creo l'istanza router e gli passo le rotte
const router = createRouter({
    // usa la web history
    history: createWebHistory(),
    routes: routes, // short for `routes: routes`
})

// creo l'applicazione Vue
const app = createApp(App);
// gli dico di usare il router
app.use(router);
// monto l'applicazione sul tag #app
app.mount('#app');